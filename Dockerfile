FROM alpine:3.14

RUN apk add --no-cache ca-certificates curl jq openssh-client

COPY gogs-notify /

ENTRYPOINT ["/gogs-notify"]
